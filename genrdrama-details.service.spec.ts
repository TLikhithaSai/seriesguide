import { TestBed } from '@angular/core/testing';

import { GenrdramaDetailsService } from './genrdrama-details.service';

describe('GenrdramaDetailsService', () => {
  let service: GenrdramaDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenrdramaDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
