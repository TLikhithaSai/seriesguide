import { TestBed } from '@angular/core/testing';

import { RomdramaDetailsService } from './romdrama-details.service';

describe('RomdramaDetailsService', () => {
  let service: RomdramaDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RomdramaDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
