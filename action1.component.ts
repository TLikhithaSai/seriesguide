import { Component, OnInit } from '@angular/core';
import { ActdramaDetailsService } from 'src/app/services/actdrama-details.service';
@Component({
  selector: 'app-action1',
  templateUrl: './action1.component.html',
  styleUrls: ['./action1.component.css']
})
export class Action1Component implements OnInit
{
  constructor(private service:ActdramaDetailsService) { }
  adData:any;
  ngOnInit(): void {
    this.adData = this.service.actdramaDetails;
  }

}
